<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\DoctorsController as AdminDoctorsController;
use App\Http\Controllers\Admin\SpecialitiesController;
use App\Http\Controllers\Auth\AuthGoogleController;
use App\Http\Controllers\Auth\AuthGoogleDoctorController;
use App\Http\Controllers\Auth\DoctorLoginController;
use App\Http\Controllers\Auth\DoctorRegisterController;
use App\Http\Controllers\Auth\LoginAdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Front\DoctorsController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*.......... Front Routes ..........*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::prefix('doctor/')->name('doctor.')->group(function () {
    Route::get('dashboard', [DoctorsController::class, 'getPanel'])->name('dashboard');
    Route::get('profile', [DoctorsController::class, 'index'])->name('profile');
    Route::post('setinfo/{id}', [DoctorsController::class, 'setInformation'])->name('save-info');
});



/*.......... Admininstrator Panel Routes ..........*/
Route::prefix('admin')->middleware('admin')->name('admin.')->group(function () {
    Route::get('/', [AdminController::class, 'index'])->name('dashboard');
    Route::get('/profile', [AdminController::class, 'getProfile'])->name('profile');
    Route::post('/profile', [AdminController::class, 'update'])->name('edit-profile');
    Route::post('/password', [AdminController::class, 'changePassword'])->name('change-password');
    Route::get('/doctors', [AdminDoctorsController::class, 'index'])->name('doctors');
    Route::resources([
        'specialities' => SpecialitiesController::class
    ]);
});

/*.......... Authentication Routes With Google ..........*/
Route::prefix('auth')->name('auth.')->group(function () {
    Route::get('/google', [AuthGoogleController::class, 'redirect'])->name('google');
    Route::get('/google/callback', [AuthGoogleController::class, 'callback'])->name('google.callback');
});

/*.......... Authentication Routes Doctor & Admin ..........*/
Route::prefix('register')->name('register.')->group(function () {
    Route::get('/doctor', [RegisterController::class, 'showDoctorRegistrationForm'])->name('form-doctor');
    Route::post('/doctor', [RegisterController::class, 'createDoctor'])->name('doctor');
});
Route::prefix('login')->name('login.')->group(function () {
    Route::get('/doctor', [LoginController::class, 'showDoctorLoginForm'])->name('form-doctor');
    Route::post('/doctor', [LoginController::class, 'doctorLogin'])->name('doctor');
    Route::get('/admin', [LoginAdminController::class, 'showLoginForm'])->name('form-admin');
    Route::post('/admin', [LoginAdminController::class, 'login'])->name('admin');
});

Auth::routes(['verify' => true]);
