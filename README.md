# online-appointments

#### Online medical appointments project

- [x] Add site template to project
- [x] User registration / login system + authentication with google
- [x] Doctor registration / login system
- [x] Admin login system
- [x] Admin profile settings
- [x] Management of specialties
- [x] Profile settings (Doctor Panel)