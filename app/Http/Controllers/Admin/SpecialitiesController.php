<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateAddSpeciality;
use App\Http\Requests\ValidateEditSpeciality;
use App\Models\Specialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\returnSelf;

class SpecialitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialities = Specialty::paginate(5);
        return view('administrator.specialities.list', compact(['specialities']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateAddSpeciality $request)
    {
        $title = $request->title;
        $image = $request->file('image');
        $name_image = rand(1, 1000) . "-img." . $image->getClientOriginalExtension();
        $image->storeAs('photos', $name_image, ['disk' => 'public']);
        $specialty = Specialty::create([
            'title' => $title,
            'image' => $name_image
        ]);
        if ($specialty) {
            Session::flash('speciality-saved', "تخصص $title با موفقیت ذخیره شد");
            return response()->json(['success' => true]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateEditSpeciality $request, $id)
    {
        $spe = Specialty::findOrFail($id);
        if ($request->file('image')) {
            Storage::disk('public')->delete('photos/' . $spe->image);
            $image = $request->file('image');
            $name_image = rand(1, 1000) . "-img." . $image->getClientOriginalExtension();
            $image->storeAs('photos', $this->checkUniqueImage($name_image, $image), ['disk' => 'public']);
            $spe->image = $name_image;
        }
        $spe->title = $request->title;
        if ($spe->save()){
            Session::flash('speciality-updated', "تخصص با موفقیت ویرایش شد");
            return response()->json(['success' => true]);
        }
    }

    // Check Unique Image
    public function checkUniqueImage($name, $image)
    {
        if (Specialty::where('image', $name)->exists()){
            return rand(1, 1000) . "-img." . $image->getClientOriginalExtension();
        }
        return $name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $spe = Specialty::findOrFail($id);
        $title = $spe->title;
        Storage::disk('public')->delete('photos/' . $spe->image);
        if ($spe->delete()){
            Session::flash('speciality-deleted', "تخصص $title با موفقیت حذف شد");
            return back();
        }
    }
}
