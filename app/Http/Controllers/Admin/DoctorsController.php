<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{

    public function index()
    {
        $doctors = Doctor::all();
        return view('administrator.doctors.list', compact(['doctors']));
    }
}
