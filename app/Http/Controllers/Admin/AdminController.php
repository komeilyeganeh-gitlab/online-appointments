<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateFormAdmin;
use App\Models\Admin;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    // Show Dashboard
    public function index()
    {
        return view('administrator.dashboard');
    }

    // Show Profile
    public function getProfile()
    {
        $user = Auth::guard('admin')->user();
        $admin = Admin::where('email', $user->email)->first();
        return view('administrator.profile', compact(['admin']));
    }

    // Update Profile
    public function update(ValidateFormAdmin $request)
    {
        $admin = Admin::where('email', $request->email)->update([
            'name' => $request->name,
            'birthday' => $request->birthday,
            'email' => $request->email,
            'phone' => $request->phone,
            'about_me' => $request->about,
            'address' => $request->address,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->postal_code,
            'country' => $request->country,
        ]);
        return response()->json($admin);
    }

    // Change Password
    public function changePassword(Request $request)
    {
        Validator::make($request->all(), [
            'oldpass' => ['required', new MatchOldPassword],
            'password' => ['bail','required','min:8', 'same:confirm_password']
        ], [
            'oldpass.required' => 'رمز عبور قبلی را وارد کنید',
            'password.required' => 'رمز عبور جدید را وارد کنید',
            'password.min' => 'رمز عبور نباید کمتر از 8 کاراکتر باشد',
            'password.same' => 'رمز عبور با تاییدیه مطابقت ندارد'
        ])->validate();
        
        $admin = Admin::where('email', auth('admin')->user()->email)->first();
        $admin->password = Hash::make($request->password);
        if ($admin->save()){
            return response()->json(['success' => 'رمز عبور با موفقیت تغییر یافت']);
        }
    }
}
