<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
         try
         {
            $google_user = Socialite::driver('google')->stateless()->user();
            $user = User::where('email', $google_user->email)->first();
            if ($user){
                Auth::loginUsingId($user->id);
            }else{
                $new_user = User::create([
                    'name' => $google_user->name,
                    'email' => $google_user->email,
                    'password' => bcrypt(Str::random(16))
                ]);
                Auth::loginUsingId($new_user->id);
            }
            return redirect(route('home'));
         }
         catch(Exception $e){
            return "Error! Not Registered";
         }
    }
}
