<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateInfoDoctor;
use App\Models\Clinic;
use App\Models\Doctor;
use App\Models\Specialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PhpParser\Comment\Doc;

class DoctorsController extends Controller
{
    // ---------- get to profile view
    public function index()
    {
        $doctor = Doctor::where('email', Auth::guard('doctor')->user()->email)->first();
        $specialities = Specialty::all();
        return view('front.doctor.profile', compact(['doctor', 'specialities']));
    }

    // ---------- get to panel view
    public function getPanel()
    {
        return view('front.doctor.dashboard');
    }

    // ---------- save & update information
    public function setInformation(ValidateInfoDoctor $request, $id)
    {
        $doctor = Doctor::where('id', $id)->first();
        if ($avatar = $request->file('avatar')) {
            if ($img = $doctor->avatar) {
                Storage::disk('public')->delete('photos/doctor/' . $img);
            }
            $nameAvatar = $this->generateNameRandom($avatar);
            $avatar->storeAs('photos/doctor/', $nameAvatar, ['disk' => 'public']);
            $doctor->avatar = $nameAvatar;
        }
        if ($request->file('image_clinic')) {
            $clinic = new Clinic();
            $clinicImage = $request->file('image_clinic');
            $nameImage = 'clinic-' . rand(1, 100000) . '.' . $clinicImage->getClientOriginalExtension();
            $clinicImage->storeAs('photos/clinics/', $nameImage, ['disk' => 'public']);
            $clinic->name = $request->name_clinic;
            $clinic->address = $request->address_clinic;
            $clinic->image = $nameImage;
            $clinic->save();
            $doctor->clinic_id = $clinic->id;
        }
        
        $doctor->name = $request->name;
        $doctor->phone = $request->phone;
        $doctor->gender = $request->gender;
        $doctor->birthday = $request->birthday;
        $doctor->speciality = $request->speciality;
        $doctor->biographi = $request->biographi;
        $doctor->address = $request->address;
        $doctor->city = $request->city;
        $doctor->province = $request->province;
        $doctor->country = $request->country;
        $doctor->postal_code = $request->postal_code;
        $doctor->status = 1;
        if ($doctor->save())
            Session::flash('update-doctor', 'اطلاعات با موفقیت ذخیره شد');
        return back();
    }

    public function generateNameRandom($image)
    {
        $name = 'avatar-' . rand(1, 100000) . '.' . $image->getClientOriginalExtension();
        while (Clinic::where('image', $name)->exists()) {
            $name = 'avatar-' . rand(1, 100000) . '.' . $image->getClientOriginalExtension();
        }
        return $name;
    }
}
