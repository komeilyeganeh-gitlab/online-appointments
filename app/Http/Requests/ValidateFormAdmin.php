<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateFormAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'birthday' => ['required', 'regex:/[0-9]{4}\/[0-9]{2}\/[0-9]{2}/'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'size:11'],
            'about' => ['required'],
            'address' => ['required'],
            'city' => ['required'],
            'province' => ['required'],
            'country' => ['required'],
            'postal_code' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'نام را وارد کنید',
            'birthday.required' => 'تاریخ تولد را وارد کنید',
            'birthday.regex' => 'فرمت تاریخ تولد صحیح نمی باشد',
            'email.required' => 'ایمیل را وارد کنید',
            'email.email' => 'فرمت ایمیل صیحیح نمی باشد',
            'phone.required' => 'شماره موبایل را وارد کنید',
            'phone.size' => 'شماره موبایل باید 11 رقمی باشد',
            'about.required' => 'متن درباره من را وارد کنید',
            'address.required' => 'آدرس را وارد کنید',
            'city.required' => 'شهر را وارد کنید',
            'province.required' => 'استان را وارد کنید',
            'country.required' => 'کشور را وارد کنید',
            'postal_code.required' => 'کد پستی را وارد کنید',
        ];
    }
}
