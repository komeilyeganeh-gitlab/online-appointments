<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use function PHPSTORM_META\map;

class ValidateInfoDoctor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => ['nullable', 'image', 'max:200000', 'mimes:png,jpg,jpeg'],
            'name' => ['required', 'string'],
            'phone' => ['required', 'numeric', 'digits:11'],
            'gender' => ['required'],
            'birthday' => ['nullable', 'regex:/[0-9]{4}\/[0-9]{2}\/[0-9]{2}/'],
            'biographi' => ['required'],
            'name_clinic' => ['nullable', 'required_with:image_clinic'],
            'address_clinic' => ['nullable', 'required_with:image_clinic'],
            'image_clinic' => ['nullable', 'required_with:name_clinic,address_clinic', 'image', 'max:200000'],
            'address' => ['required'],
            'city' => ['required'],
            'province' => ['required'],
            'postal_code' => ['nullable', 'numeric', 'digits:10', 'unique:doctors,postal_code'],
            'speciality' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'avatar.image' => 'فایل باید تصویر باشد',
            'avatar.max' => 'حجم فایل بیش از ۲ مگابایت است',
            'avatar.mimes' => 'فرمت فایل اشتباه می باشد',
            'name.required' => 'نام را وارد کنید',
            'name.string' => 'نام باید رشته باشد',
            'phone.required' => 'شماره تماس را وارد کنید ',
            'phone.numeric' => 'شماره تماس باید عددی باشد',
            'phone.digits' => 'شماره تماس باید ۱۱ رقمی باشد',
            'gender.required' => 'جنسیت را انتخاب کنید ',
            'birthday.required' => 'تاریخ تولد باید به فرمت xxxx/xx/xx باشد',
            'biographi.required' => 'بیوگرافی خودرا بنویسید',
            'name_clinic.required_with' => 'زمانی که تصویر بارگزاری شده نام کلینیک هم باید وارد شود',
            'address_clinic.required_with' => 'آدرس کلینیک را وارد کنید',
            'image_clinic.required_with' => 'تصویر کلینیک را بارگزاری کنید',
            'image_clinic.image' => 'فایل باید تصویر باشد',
            'image_clinic.max' => 'حجم فایل بیش از 2 مگابایت می باشد',
            'address.required' => 'آدرس خودرا وارد کنید ',
            'city.required' => 'شهر خودرا وارد کنید',
            'province.required' => 'استان خودرا وارد کنید',
            'postal_code.numeric' => 'کدپستی باید عددی باشد',
            'postal_code.digits' => 'کدپستی باید ۱۰ رقمی باشد',
            'postal_code.unique' => 'این کدپستی قبلا ثبت شده است',
            'speciality.required' => 'تخصص را انتخاب کنید'
        ];
    }
}
