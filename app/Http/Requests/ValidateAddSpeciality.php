<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateAddSpeciality extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'image' => 'required|image|max:100000'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'عنوان تخصص را وارد کنید',
            'image.required' => 'تصویر را بارگزاری کنید',
            'image.image' => 'فایل باید تصویر باشد',
            'image.max' => 'حجم فایل بیش از 1 مگابایت می باشد'
        ];
    }
}
