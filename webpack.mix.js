const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .vue()
//     .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/plugins/bootstrap-rtl/css/bootstrap.min.css',
    'resources/assets/plugins/fontawesome/css/fontawesome.min.css',
    'resources/assets/plugins/fontawesome/css/all.min.css',
    'resources/assets/plugins/select2/css/select2.min.css',
    'resources/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
    'resources/assets/plugins/dropzone/dropzone.min.css',
    'resources/assets/css/style.css',
], 'public/front/all.min.css')
.scripts([
    'resources/assets/js/jquery.min.js',
    'resources/assets/js/popper.min.js',
    'resources/assets/plugins/bootstrap-rtl/js/bootstrap.min.js',
    'resources/assets/plugins/theia-sticky-sidebar/ResizeSensor.js',
    'resources/assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js',
    'resources/assets/plugins/select2/js/select2.min.js',
    'resources/assets/plugins/dropzone/dropzone.min.js',
    'resources/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js',
    'resources/assets/js/profile-settings.js',
    'resources/assets/js/script.js',
], 'public/front/all.min.js')