@extends('layouts.master-profile')

@section('title', 'پروفایل کاربری')

@section('content')
    <!-- Breadcrumb -->
    <div class="breadcrumb-bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page">پروفایل</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title">پروفایل</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- /Breadcrumb -->

    <!-- Page Content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar dct-dashbd-lft">

                    <!-- Profile Widget -->
                    <div class="card widget-profile pat-widget-profile">
                        <div class="card-body">
                            <div class="pro-widget-content">
                                <div class="profile-info-widget">
                                    <a href="#" class="booking-doc-img">
                                        <img src="front/img/patients/patient.jpg" alt="User Image">
                                    </a>
                                    <div class="profile-det-info">
                                        <h3>ریچارد ویلسون</h3>

                                        <div class="patient-details">
                                            <h5><b>شناسه مراجعه‌کننده :</b> PT0016</h5>
                                            <h5 class="mb-0"><i class="fas fa-map-marker-alt"></i>نیویورک، آمریکا
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="patient-info">
                                <ul>
                                    <li>شماره تماس <span>+1 952 001 8563</span></li>
                                    <li>سن <span>38 سال، مرد</span></li>
                                    <li>گروه خونی<span>AB+</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Profile Widget -->

                    <!-- Last Booking -->
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">آخرین رزرو</h4>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <div class="mr-3">
                                        <img alt="Image placeholder" src="front/img/doctors/doctor-thumb-02.jpg"
                                            class="avatar  rounded-circle">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="d-block mb-0">دکتر دارن الدور </h5>
                                        <span class="d-block text-sm text-muted">دندان‌پزشک</span>
                                        <span class="d-block text-sm text-muted">14 مهر 1399 5.00 بعد از ظهر</span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <div class="mr-3">
                                        <img alt="Image placeholder" src="front/img/doctors/doctor-thumb-02.jpg"
                                            class="avatar  rounded-circle">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="d-block mb-0">دکتر دارن الدور </h5>
                                        <span class="d-block text-sm text-muted">دندان‌پزشک</span>
                                        <span class="d-block text-sm text-muted">12 مهر 1399 11.00 قبل از ظهر</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /Last Booking -->

                </div>

                <div class="col-md-7 col-lg-8 col-xl-9 dct-appoinment">
                    <div class="card">
                        <div class="card-body pt-0">
                            <div class="user-tabs">
                                <ul class="nav nav-tabs nav-tabs-bottom nav-justified flex-wrap">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#pat_appointments" data-toggle="tab">نوبت‌دهی</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#pres" data-toggle="tab"><span> نسخه</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#medical" data-toggle="tab"><span
                                                class="med-records"> سوابق پزشکی </span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#billing" data-toggle="tab"><span> صورتحساب
                                            </span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">

                                <!-- Appointment Tab -->
                                <div id="pat_appointments" class="tab-pane fade show active">
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>پزشک</th>
                                                            <th>تاریخ نوبت</th>
                                                            <th> تاریخ رزرو </th>
                                                            <th>مقدار</th>
                                                            <th> پیگیری </th>
                                                            <th> وضعیت </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>14 مهر 1399 <span class="d-block text-info">10.00 قبل از
                                                                    ظهر</span></td>
                                                            <td>12 مهر 1399</td>
                                                            <td>160 تومان</td>
                                                            <td>16 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-success-light"> تأیید
                                                                </span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>12 مهر 1399 <span class="d-block text-info">8.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>12 مهر 1399</td>
                                                            <td>250 تومان</td>
                                                            <td>14 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-success-light"> تأیید
                                                                </span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>11 مهر 1399 <span class="d-block text-info">11.00 قبل از
                                                                    ظهر</span></td>
                                                            <td>10 مهر 1399</td>
                                                            <td>400 تومان</td>
                                                            <td>13 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-danger-light">لغو</span>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>10 مهر 1399 <span class="d-block text-info">3.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>10 مهر 1399</td>
                                                            <td>350 تومان</td>
                                                            <td>12 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-warning-light"> معلق
                                                                </span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="edit-prescription.html"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-danger-light">
                                                                        <i class="far fa-trash-alt"></i> حذف
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>9 مهر 1399 <span class="d-block text-info">7.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>8 مهر 1399</td>
                                                            <td>75 تومان</td>
                                                            <td>11 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-success-light"> تأیید
                                                                </span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>8 مهر 1399 <span class="d-block text-info">9.00 قبل از
                                                                    ظهر</span></td>
                                                            <td>6 مهر 1399</td>
                                                            <td>175 تومان</td>
                                                            <td>10 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-danger-light">لغو</span>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>8 مهر 1399 <span class="d-block text-info">6.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>6 مهر 1399</td>
                                                            <td> 450 تومان </td>
                                                            <td>10 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-success-light"> تأیید
                                                                </span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="far fa-edit"></i> ویرایش
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>7 مهر 1399 <span class="d-block text-info">9.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>7 مهر 1399</td>
                                                            <td> 275 تومان </td>
                                                            <td>9 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-info-light">تکمیل
                                                                    شده</span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="far fa-clock"></i> برنامه ریزی مجدد
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>6 مهر 1399 <span class="d-block text-info">8.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>4 مهر 1399</td>
                                                            <td>600 تومان</td>
                                                            <td>8 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-info-light">تکمیل
                                                                    شده</span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="far fa-clock"></i> برنامه ریزی مجدد
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>5 مهر 1399 <span class="d-block text-info">5.00 بعد از
                                                                    ظهر</span></td>
                                                            <td>5 مهر 1399</td>
                                                            <td>100 تومان</td>
                                                            <td>7 مهر 1399</td>
                                                            <td><span class="badge badge-pill bg-info-light">تکمیل
                                                                    شده</span></td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="far fa-clock"></i> برنامه ریزی مجدد
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Appointment Tab -->

                                <!-- Prescription Tab -->
                                <div class="tab-pane fade" id="pres">
                                    <div class="text-right">
                                        <a href="add-prescription.html" class="add-new-btn">افزودن مراجعه‌کننده</a>
                                    </div>
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th> تاریخ</th>
                                                            <th>نام</th>
                                                            <th>ایجاد شده توسط</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>14 مهر 1399</td>
                                                            <td> نسخه 1</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-01.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک رابی پرین <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>13 مهر 1399</td>
                                                            <td> نسخه 2</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                    <a href="edit-prescription.html"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="fas fa-edit"></i> ویرایش
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-danger-light">
                                                                        <i class="far fa-trash-alt"></i> حذف </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>12 مهر 1399</td>
                                                            <td> نسخه 3</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-03.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک دبرا آنجل <span>قلب و
                                                                            عروق</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>11 مهر 1399</td>
                                                            <td> نسخه 4</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-04.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک سوفیا برنت
                                                                        <span>اورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>10 مهر 1399</td>
                                                            <td> نسخه 5</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-05.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک ماروین کمپل
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>9 مهر 1399</td>
                                                            <td> نسخه 6</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-06.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک کاترینا برتفولد
                                                                        <span>ارتوپدی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>8 مهر 1399</td>
                                                            <td> نسخه 7</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-07.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک لیندا توبین
                                                                        <span>نئورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>7 مهر 1399</td>
                                                            <td> نسخه 8</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-08.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک پول ریچارد
                                                                        <span>پوست</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6 مهر 1399</td>
                                                            <td> نسخه 9</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-09.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک جان گیبز <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5 مهر 1399</td>
                                                            <td> نسخه 10</td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-10.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک اولگا بارلو
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Prescription Tab -->

                                <!-- Medical Records Tab -->
                                <div class="tab-pane fade" id="medical">
                                    <div class="text-right">
                                        <a href="#" class="add-new-btn" data-toggle="modal"
                                            data-target="#add_medical_records"> افزودن سوابق پزشکی </a>
                                    </div>
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>شناسه</th>
                                                            <th> تاریخ</th>
                                                            <th> توضیحات </th>
                                                            <th> پیوست </th>
                                                            <th> ایجاد شده </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0010</a></td>
                                                            <td>14 مهر 1399</td>
                                                            <td>پر کردن دندان</td>
                                                            <td><a href="#">dental-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-01.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک رابی پرین <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0009</a></td>
                                                            <td>13 مهر 1399</td>
                                                            <td>جرم گیری</td>
                                                            <td><a href="#">dental-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                    <a href="edit-prescription.html"
                                                                        class="btn btn-sm bg-success-light"
                                                                        data-toggle="modal"
                                                                        data-target="#add_medical_records">
                                                                        <i class="fas fa-edit"></i> ویرایش
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-danger-light">
                                                                        <i class="far fa-trash-alt"></i> حذف </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0008</a></td>
                                                            <td>12 مهر 1399</td>
                                                            <td>چکاپ</td>
                                                            <td><a href="#">cardio-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-03.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک دبرا آنجل <span>قلب و
                                                                            عروق</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0007</a></td>
                                                            <td>11 مهر 1399</td>
                                                            <td>تست کلی</td>
                                                            <td><a href="#">general-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-04.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک سوفیا برنت
                                                                        <span>اورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0006</a></td>
                                                            <td>10 مهر 1399</td>
                                                            <td>تست چشم</td>
                                                            <td><a href="#">eye-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-05.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک ماروین کمپل <span>چشم
                                                                            پزشکی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0005</a></td>
                                                            <td>9 مهر 1399</td>
                                                            <td>درد پا</td>
                                                            <td><a href="#">ortho-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-06.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک کاترینا برتفولد
                                                                        <span>ارتوپدی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0004</a></td>
                                                            <td>8 مهر 1399</td>
                                                            <td>سردرد</td>
                                                            <td><a href="#">neuro-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-07.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک لیندا توبین
                                                                        <span>نئورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0003</a></td>
                                                            <td>7 مهر 1399</td>
                                                            <td>آلرژی پوست</td>
                                                            <td><a href="#">alergy-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-08.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک پول ریچارد
                                                                        <span>پوست</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0002</a></td>
                                                            <td>6 مهر 1399</td>
                                                            <td>کشیدن دندان</td>
                                                            <td><a href="#">dental-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-09.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک جان گیبز <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:void(0);">#MR-0001</a></td>
                                                            <td>5 مهر 1399</td>
                                                            <td>پر کردن دندان</td>
                                                            <td><a href="#">dental-test.pdf</a></td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-10.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک اولگا بارلو
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Medical Records Tab -->

                                <!-- Billing Tab -->
                                <div class="tab-pane" id="billing">
                                    <div class="text-right">
                                        <a class="add-new-btn" href="add-billing.html">افزودن صورت حساب</a>
                                    </div>
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">

                                                <table class="table table-hover table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>شماره فاکتور</th>
                                                            <th>پزشک</th>
                                                            <th>مقدار</th>
                                                            <th>پرداخت شده</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0010</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-01.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">رابی پرین <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td> 450 تومان </td>
                                                            <td>14 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0009</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-02.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">دکتر دارن الدور
                                                                        <span>دندان پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>300 تومان</td>
                                                            <td>13 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                    <a href="edit-billing.html"
                                                                        class="btn btn-sm bg-success-light">
                                                                        <i class="fas fa-edit"></i> ویرایش
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-danger-light">
                                                                        <i class="far fa-trash-alt"></i> حذف </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0008</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-03.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک دبرا آنجل <span>قلب و
                                                                            عروق</span></a>
                                                                </h2>
                                                            </td>
                                                            <td> 150 تومان </td>
                                                            <td>12 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0007</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-04.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک سوفیا برنت
                                                                        <span>اورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>50 تومان</td>
                                                            <td>11 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0006</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-05.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک ماروین کمپل <span>چشم
                                                                            پزشکی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>600 تومان</td>
                                                            <td>10 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0005</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-06.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک کاترینا برتفولد
                                                                        <span>ارتوپدی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>200 تومان</td>
                                                            <td>9 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0004</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-07.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک لیندا توبین
                                                                        <span>نئورولوژی</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>100 تومان</td>
                                                            <td>8 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0003</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-08.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک پول ریچارد
                                                                        <span>پوست</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>250 تومان</td>
                                                            <td>7 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0002</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-09.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک جان گیبز <span>دندان
                                                                            پزشک</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>175 تومان</td>
                                                            <td>6 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="invoice-view.html">#INV-0001</a>
                                                            </td>
                                                            <td>
                                                                <h2 class="table-avatar">
                                                                    <a href="doctor-profile.html"
                                                                        class="avatar avatar-sm mr-2">
                                                                        <img class="avatar-img rounded-circle"
                                                                            src="front/img/doctors/doctor-thumb-10.jpg"
                                                                            alt="User Image">
                                                                    </a>
                                                                    <a href="doctor-profile.html">پزشک اولگا بارلو
                                                                        <span>#0010</span></a>
                                                                </h2>
                                                            </td>
                                                            <td>550 تومان</td>
                                                            <td>5 مهر 1399</td>
                                                            <td class="text-right">
                                                                <div class="table-action">
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-primary-light">
                                                                        <i class="fas fa-print"></i> چاپ
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                        class="btn btn-sm bg-info-light">
                                                                        <i class="far fa-eye"></i> مشاهده
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Billing Tab -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
@endsection
