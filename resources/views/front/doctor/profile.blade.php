@extends('layouts.master-profile')

@section('title', 'پروفایل پزشک')

@section('content')
    <div class="breadcrumb-bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page">تنظیمات پروفایل</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title">تنظیمات پروفایل</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">

                    <!-- Profile Sidebar -->
                    <div class="profile-sidebar">
                        <div class="widget-profile pro-widget-content">
                            <div class="profile-info-widget">
                                <a href="#" class="booking-doc-img">
                                    <img src="{{ $doctor->avatar ? asset('storage/photos/doctor/' . $doctor->avatar) : asset('front/img/avatar-default-icon.png') }}"
                                        alt="User Image">
                                </a>
                                <div class="profile-det-info">
                                    <h3>{{ $doctor->name }}</h3>

                                    <div class="patient-details">
                                        <h5 class="mb-0"> BDS ، MDS - جراحی دهان و فک و صورت </h5>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div>
                                <p class="font-weigth-bold">وضعیت :
                                    @if ($doctor->status == 0)
                                        <span class="font-weigth-bold text-white p-1 badge badge-danger">غیرفعال</span>
                                        <br>
                                        <small class="text-secondary">برای فعال شدن حساب کاربری باید اطلاعات پروفایل خود را
                                            کامل کنید</small>
                                    @else
                                        <span class="font-weigth-bold text-white p-1 badge badge-success">فعال</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="dashboard-widget">
                            <nav class="dashboard-menu">
                                <ul>
                                    <li>
                                        <a href="doctor-dashboard.html">
                                            <i class="fas fa-columns"></i>
                                            <span>داشبورد</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="appointments.html">
                                            <i class="fas fa-calendar-check"></i>
                                            <span>نوبت‌دهی</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="my-patients.html">
                                            <i class="fas fa-user-injured"></i>
                                            <span>مراجعه‌کنندگان من</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="schedule-timings.html">
                                            <i class="fas fa-hourglass-start"></i>
                                            <span>زمان‌بندی</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="invoices.html">
                                            <i class="fas fa-file-invoice"></i>
                                            <span>صورت‌حساب</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="reviews.html">
                                            <i class="fas fa-star"></i>
                                            <span>نظرات</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="chat-doctor.html">
                                            <i class="fas fa-comments"></i>
                                            <span>پیام‌ها</span>
                                            <small class="unread-msg">23</small>
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a href="doctor-profile-settings.html">
                                            <i class="fas fa-user-cog"></i>
                                            <span>تنظیمات پروفایل</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="social-media.html">
                                            <i class="fas fa-share-alt"></i>
                                            <span>شبکه‌های اجتماعی</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="doctor-change-password.html">
                                            <i class="fas fa-lock"></i>
                                            <span>‌تغییر رمز عبور</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            <i class="fas fa-sign-out-alt"></i>
                                            <span>خروج</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- /Profile Sidebar -->

                </div>
                <div class="col-md-7 col-lg-8 col-xl-9">
                    <form action="{{ route('doctor.save-info', $doctor->id) }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf

                        <!-- Basic Information -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">اطلاعات پایه</h4>
                                @if (Session::has('update-doctor'))
                                    <div class="alert alert-success">{{ Session::get('update-doctor') }}</div>
                                @endif
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="change-avatar">
                                                <div class="profile-img">
                                                    <img src="{{ $doctor->avatar ? asset('storage/photos/doctor/' . $doctor->avatar) : asset('front/img/avatar-default-icon.png') }}"
                                                        alt="User Image">
                                                </div>
                                                <div class="upload-img">
                                                    <div class="change-photo-btn">
                                                        <span><i class="fa fa-upload"></i> بارگذاری تصویر </span>
                                                        <input type="file" class="upload" name="avatar">
                                                    </div>
                                                    @error('avatar')
                                                        <div class="alert alert-danger p-1 small">{{ $message }}</div>
                                                    @enderror
                                                    <small class="form-text text-muted">JPG ، JPEG یا PNG مجاز است. حداکثر
                                                        اندازه
                                                        2 مگابایت</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>نام کاربری <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" readonly
                                                value="{{ $doctor->username }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ایمیل<span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" readonly
                                                value="{{ $doctor->email }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>نام و نام خانوادگی<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="name"
                                                value="{{ $doctor->name ?? old('name') }}">
                                            @error('name')
                                                <div class="alert alert-danger p-1 small">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>شماره تماس <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="phone"
                                                value="{{ $doctor->phone ?? old('phone') }}">
                                            @error('phone')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>جنسیت <span class="text-danger">*</span></label>
                                            <select class="form-control select" name="gender">
                                                <option value="">انتخاب</option>
                                                <option value="man" @if($doctor->gender == 'man') selected @endif>مرد
                                                </option>
                                                <option value="female" @if($doctor->gender == 'female') selected @endif}>
                                                    زن</option>
                                            </select>
                                            @error('gender')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label>تاریخ تولد</label>
                                            <input type="text" class="form-control" name="birthday"
                                                value="{{ $doctor->birthday ?? old('birthday') }}">
                                            @error('birthday')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Basic Information -->

                        <!-- Specialization -->
                        <div class="card services-card">
                            <div class="card-body">
                                <h4 class="card-title">تخصص <span class="text-danger">*</span></h4>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control select" name="speciality">
                                            <option value="" @if ($doctor->speciality == "") selected @endif>انتخاب</option>
                                            @foreach ($specialities as $spe)
                                                <option value="{{ $spe->title }}" @if ($doctor->speciality == $spe->title) selected @endif>{{ $spe->title }}</option>
                                            @endforeach
                                        </select>
                                        @error('speciality')
                                            <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Specialization -->

                        <!-- About Me -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"> درباره من <span class="text-danger">*</span></h4>
                                <div class="form-group mb-0">
                                    <label>بیوگرافی</label>
                                    <textarea class="form-control" rows="5"
                                        name="biographi">{{ $doctor->biographi ?? old('biographi') }}</textarea>
                                    @error('biographi')
                                        <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /About Me -->

                        <!-- Clinic Info -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">اطلاعات کلینیک</h4>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>نام کلینیک</label>
                                            <input type="text" class="form-control" name="name_clinic"
                                                value="{{ $doctor->clinic->name ?? old('name_clinic') }}">
                                            @error('name_clinic')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>آدرس کلینیک</label>
                                            <input type="text" class="form-control" name="address_clinic"
                                                value="{{ $doctor->clinic->address ?? old('address_clinic') }}">
                                            @error('address_clinic')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>تصویر کلینیک</label>
                                            <input type="file" class="form-control" name="image_clinic">
                                            @error('image_clinic')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="upload-wrap">
                                            <div class="upload-images">
                                                <img src="assets/img/features/feature-01.jpg" alt="Upload Image">
                                                <a href="javascript:void(0);" class="btn btn-icon btn-danger btn-sm"><i
                                                        class="far fa-trash-alt"></i></a>
                                            </div>
                                            <div class="upload-images">
                                                <img src="assets/img/features/feature-02.jpg" alt="Upload Image">
                                                <a href="javascript:void(0);" class="btn btn-icon btn-danger btn-sm"><i
                                                        class="far fa-trash-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Clinic Info -->

                        <!-- Contact Details -->
                        <div class="card contact-card">
                            <div class="card-body">
                                <h4 class="card-title">جزییات تماس</h4>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>آدرس <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="address"
                                                value="{{ $doctor->address ?? old('address') }}">
                                            @error('address')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">شهر <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="city"
                                                value="{{ $doctor->city ?? old('city') }}">
                                            @error('city')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">استان <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="province"
                                                value="{{ $doctor->province ?? old('province') }}">
                                            @error('province')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">کشور</label>
                                            <input type="text" class="form-control" name="country"
                                                value="{{ $doctor->country ?? old('country') }}">
                                            @error('country')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">کدپستی</label>
                                            <input type="text" class="form-control" name="postal_code"
                                                value="{{ $doctor->postal_code ?? old('postal_code') }}">
                                            @error('postal_code')
                                                <div class="alert alert-danger p-1 small mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Contact Details -->


                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn">ذخیره تغییرات</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
