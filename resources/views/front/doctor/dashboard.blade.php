@extends('layouts.master-profile')

@section('title', 'پنل مدیریت')

@section('content')
    <div class="breadcrumb-bar">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12 col-12">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">خانه</a></li>
                            <li class="breadcrumb-item active" aria-current="page">دشبرد</li>
                        </ol>
                    </nav>
                    <h2 class="breadcrumb-title">دشبرد</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="content" style="transform: none; min-height: 329px;">
        <div class="container-fluid" style="transform: none;">

            <div class="row" style="transform: none;">
                <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar"
                    style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                    <!-- Profile Sidebar -->

                    <!-- /Profile Sidebar -->

                    <div class="theiaStickySidebar"
                        style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 1171.5px;">
                        <div class="profile-sidebar">
                            <div class="widget-profile pro-widget-content">
                                <div class="profile-info-widget">
                                    <a href="#" class="booking-doc-img">
                                        <img src="{{ asset('front/img/doctors/doctor-thumb-02.jpg') }}" alt="User Image">
                                    </a>
                                    <div class="profile-det-info">
                                        <h3>پزشک دارن الدر</h3>

                                        <div class="patient-details">
                                            <h5 class="mb-0"> BDS ، MDS - جراحی دهان و فک و صورت </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dashboard-widget">
                                <nav class="dashboard-menu">
                                    <ul>
                                        <li class="active">
                                            <a href="doctor-dashboard.html">
                                                <i class="fas fa-columns"></i>
                                                <span>دشبرد</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="appointments.html">
                                                <i class="fas fa-calendar-check"></i>
                                                <span>نوبت‌دهی</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="my-patients.html">
                                                <i class="fas fa-user-injured"></i>
                                                <span>مراجعه‌کنندگان من</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="schedule-timings.html">
                                                <i class="fas fa-hourglass-start"></i>
                                                <span>زمان‌بندی</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="invoices.html">
                                                <i class="fas fa-file-invoice"></i>
                                                <span>صورت‌حساب</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="reviews.html">
                                                <i class="fas fa-star"></i>
                                                <span>نظرات</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="chat-doctor.html">
                                                <i class="fas fa-comments"></i>
                                                <span>پیام‌ها</span>
                                                <small class="unread-msg">23</small>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="doctor-profile-settings.html">
                                                <i class="fas fa-user-cog"></i>
                                                <span>تنظیمات پروفایل</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="social-media.html">
                                                <i class="fas fa-share-alt"></i>
                                                <span>شبکه‌های اجتماعی</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="doctor-change-password.html">
                                                <i class="fas fa-lock"></i>
                                                <span>‌تغییر رمز عبور</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html">
                                                <i class="fas fa-sign-out-alt"></i>
                                                <span>خروج</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="resize-sensor"
                            style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
                            <div class="resize-sensor-expand"
                                style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div
                                    style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 391px; height: 876px;">
                                </div>
                            </div>
                            <div class="resize-sensor-shrink"
                                style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-7 col-lg-8 col-xl-9">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card dash-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-4">
                                            <div class="dash-widget dct-border-rht">
                                                <div class="circle-bar circle-bar1">
                                                    <div class="circle-graph1" data-percent="75"><canvas width="400"
                                                            height="400"></canvas>
                                                        <img src="{{ asset('front/img/icon-01.png') }}" class="img-fluid"
                                                            alt="patient">
                                                    </div>
                                                </div>
                                                <div class="dash-widget-info">
                                                    <h6> مراجعان کلی </h6>
                                                    <h3>1500</h3>
                                                    <p class="text-muted"> تا امروز </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-4">
                                            <div class="dash-widget dct-border-rht">
                                                <div class="circle-bar circle-bar2">
                                                    <div class="circle-graph2" data-percent="65"><canvas width="400"
                                                            height="400"></canvas>
                                                        <img src="{{ asset('front/img/icon-02.png') }}" class="img-fluid"
                                                            alt="Patient">
                                                    </div>
                                                </div>
                                                <div class="dash-widget-info">
                                                    <h6> مراجعان امروز </h6>
                                                    <h3>160</h3>
                                                    <p class="text-muted">6 تیر 1398</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-4">
                                            <div class="dash-widget">
                                                <div class="circle-bar circle-bar3">
                                                    <div class="circle-graph3" data-percent="50"><canvas width="400"
                                                            height="400"></canvas>
                                                        <img src="{{ asset('front/img/icon-03.png') }}" class="img-fluid"
                                                            alt="Patient">
                                                    </div>
                                                </div>
                                                <div class="dash-widget-info">
                                                    <h6>نوبت ها</h6>
                                                    <h3>85</h3>
                                                    <p class="text-muted">6 تیر 1398</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mb-4">نوبت مراجعان</h4>
                            <div class="appointment-tab">

                                <!-- Appointment Tab -->
                                <ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#upcoming-appointments" data-toggle="tab">در حال
                                            آمدن</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#today-appointments" data-toggle="tab">امروز</a>
                                    </li>
                                </ul>
                                <!-- /Appointment Tab -->

                                <div class="tab-content">

                                    <!-- Upcoming Appointment Tab -->
                                    <div class="tab-pane show active" id="upcoming-appointments">
                                        <div class="card card-table mb-0">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-center mb-0">
                                                        <thead>
                                                            <tr>
                                                                <th>نام مراجعه کننده</th>
                                                                <th>تاریخ نوبت</th>
                                                                <th>هدف</th>
                                                                <th>نوع</th>
                                                                <th class="text-center">مقدار پرداختی</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">ریچادر ریسون
                                                                            <span>#PT0016</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>11 مهر 1399 <span class="d-block text-info">10.00 قبل از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center"> 150 تومان </td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient1.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">چارلن رد
                                                                            <span>#PT0001</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>3 مهر 1399 <span class="d-block text-info">11.00 قبل از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">200 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient2.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">تراویس تریمبل
                                                                            <span>#PT0002</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>5 مهر 1399 <span class="d-block text-info">1.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center">75 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient3.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">کارل کلی
                                                                            <span>#PT0003</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>6 مهر 1399 <span class="d-block text-info">9.00 قبل از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">100 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient4.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">میچل فایرفکس
                                                                            <span>#PT0004</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>8 مهر 1399 <span class="d-block text-info">6.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center">350 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient5.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">جینا مور
                                                                            <span>#PT0005</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>9 مهر 1399 <span class="d-block text-info">8.00 قبل از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">250 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Upcoming Appointment Tab -->

                                    <!-- Today Appointment Tab -->
                                    <div class="tab-pane" id="today-appointments">
                                        <div class="card card-table mb-0">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-center mb-0">
                                                        <thead>
                                                            <tr>
                                                                <th>نام مراجعه کننده</th>
                                                                <th>تاریخ نوبت</th>
                                                                <th>هدف</th>
                                                                <th>نوع</th>
                                                                <th class="text-center">مقدار پرداختی</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient6.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">ایلس گیلی
                                                                            <span>#PT0006</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">6.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>Fever</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">300 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient7.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">جوان گاردنر
                                                                            <span>#PT0006</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">5.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">100 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient8.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">دنیل جیفرینگ
                                                                            <span>#PT0007</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">3.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center">75 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient9.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">والتر رابینسون
                                                                            <span>#PT0008</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">1.00 بعد از
                                                                        ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع قدیمی</td>
                                                                <td class="text-center">350 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient10.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">روبرت رادیز
                                                                            <span>#PT0010</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">10.00 قبل
                                                                        از ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center">175 تومان</td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="patient-profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ asset('front/img/patients/patient11.jpg') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="patient-profile.html">هری ویلیامز
                                                                            <span>#PT0011</span></a>
                                                                    </h2>
                                                                </td>
                                                                <td>14 مهر 1399 <span class="d-block text-info">11.00 قبل
                                                                        از ظهر</span></td>
                                                                <td>کلی</td>
                                                                <td>مراجع جدید</td>
                                                                <td class="text-center"> 450 تومان </td>
                                                                <td class="text-right">
                                                                    <div class="table-action">
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-info-light">
                                                                            <i class="far fa-eye"></i> مشاهده
                                                                        </a>

                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-success-light">
                                                                            <i class="fas fa-check"></i> قبول
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                            class="btn btn-sm bg-danger-light">
                                                                            <i class="fas fa-times"></i> حذف
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Today Appointment Tab -->

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
@endsection
