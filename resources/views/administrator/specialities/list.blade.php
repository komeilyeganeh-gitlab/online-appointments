@extends('administrator.layouts.master')

@section('title', 'لیست تخصص ها')

@section('content')
    <div class="page-wrapper" style="min-height: 534px;">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-7 col-auto">
                        <h3 class="page-title">تخصص ها</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">دشبرد</a></li>
                            <li class="breadcrumb-item active">تخصص ها</li>
                        </ul>
                    </div>
                    <div class="col-sm-5 col">
                        <a href="#Add_Specialities_details" data-toggle="modal"
                            class="btn btn-primary float-right mt-2">افزودن</a>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive overflow-hidden">
                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="DataTables_Table_0_length"><label>نمایش
                                                    <select name="DataTables_Table_0_length"
                                                        aria-controls="DataTables_Table_0"
                                                        class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> داده</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6"></div>
                                    </div>
                                    <div class="success-add">
                                        @if (Session::get('speciality-saved'))
                                            <div class="alert alert-success">
                                                <p class="mb-0" style="font-size: 0.9rem">
                                                    {{ Session::get('speciality-saved') }}</p>
                                            </div>
                                        @endif
                                        @if (Session::get('speciality-updated'))
                                            <div class="alert alert-success">
                                                <p class="mb-0" style="font-size: 0.9rem">
                                                    {{ Session::get('speciality-updated') }}</p>
                                            </div>
                                        @endif
                                        @if (Session::get('speciality-deleted'))
                                            <div class="alert alert-success">
                                                <p class="mb-0" style="font-size: 0.9rem">
                                                    {{ Session::get('speciality-deleted') }}</p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="datatable table table-hover table-center mb-0 dataTable no-footer"
                                                id="DataTables_Table_0" role="grid"
                                                aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 170.05px;" aria-sort="ascending"
                                                            aria-label="#: activate to sort column descending">#</th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 529.75px;"
                                                            aria-label="تخصص ها: activate to sort column ascending">تخصص ها
                                                        </th>
                                                        <th class="text-right sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 388.2px;"
                                                            aria-label=" اقدامات : activate to sort column ascending">
                                                            اقدامات </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <!-- List Specialities -->
                                                    @if (count($specialities) == 0)
                                                        <div class="alert alert-danger">
                                                            <p class="mb-0" style="font-size: 0.9rem">تخصصی وجود
                                                                ندارد</p>
                                                        </div>
                                                    @else
                                                        @foreach ($specialities as $speciality)
                                                            <tr role="row" class="odd">
                                                                <td class="sorting_1">SP-{{ $speciality->id }}</td>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <span class="avatar avatar-sm mr-2">
                                                                            <img class="avatar-img"
                                                                                src="{{ asset('storage/photos/' . $speciality->image) }}"
                                                                                alt="Speciality">
                                                                        </span>
                                                                        <p class="mb-0">{{ $speciality->title }}</p>
                                                                    </h2>
                                                                </td>
                                                                <td class="text-right">
                                                                    <div class="actions">
                                                                        <a class="btn btn-sm bg-success-light btn-edit-speciality"
                                                                            data-toggle="modal"
                                                                            href="#edit_specialities_details"
                                                                            data-id="{{ $speciality->id }}"
                                                                            data-title="{{ $speciality->title }}">
                                                                            <i class="fe fe-pencil"></i> ویرایش
                                                                        </a>
                                                                        <a data-toggle="modal"
                                                                            data-id="{{ $speciality->id }}"
                                                                            href="#delete_modal"
                                                                            class="btn btn-sm bg-danger-light btn-modal-delete">
                                                                            <i class="fe fe-trash"></i> حذف
                                                                        </a>

                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{ $specialities->links() }}
                                        {{-- <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="DataTables_Table_0_info" role="status"
                                                aria-live="polite">نمایش 1 تا 5 از 5 داده</div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                id="DataTables_Table_0_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="DataTables_Table_0_previous"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
                                                            class="page-link">قبلی</a></li>
                                                    <li class="paginate_button page-item active"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0"
                                                            class="page-link">1</a></li>
                                                    <li class="paginate_button page-item next disabled"
                                                        id="DataTables_Table_0_next"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
                                                            class="page-link">بعدی</a></li>
                                                </ul>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Specialities -->
    <div class="modal fade" id="Add_Specialities_details" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">افزودن تخصص</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="errors mt-2 mr-1 ml-1"></div>
                <div class="modal-body">
                    <form action="{{ route('admin.specialities.store') }}" method="POST" enctype="multipart/form-data"
                        id="form-add-speciality">
                        @csrf
                        <div class="row form-row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>عنوان تخصص</label>
                                    <input type="text" class="form-control" name="title" id="title">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>تصویر</label>
                                    <input type="file" class="form-control" name="image" id="image">
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary btn-block">ذخیره</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Specialities -->
    <div class="modal fade" id="edit_specialities_details" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ویرایش تخصص</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="errors-edit mt-2 mr-1 ml-1"></div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" class="form-edit" method="POST" id="form-edit-speciality">
                        @csrf
                        @method('PATCH')
                        <div class="row form-row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>عنوان تخصص</label>
                                    <input type="text" class="form-control title" name="title">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>تصویر</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary btn-block">ذخیره تغییرات</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Specialities -->
    <div class="modal fade" id="delete_modal" role="dialog" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <!--	<div class="modal-header">
                            <h5 class="modal-title">حذف</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>-->
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">حذف</h4>
                        <p class="mb-4">آیا مطمئن هستید که می خواهید حذف کنید؟</p>
                        <form id="form-delete-speciality" class="d-inline" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary">بله</button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">خیر</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('administrator/js/speciality.js') }}"></script>
@endsection
