@extends('administrator.layouts.master')

@section('title', 'پزشکان')

@section('content')
    <div class="page-wrapper" style="min-height: 534px;">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title"> لیست پزشکان </h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">دشبرد</a></li>
                            <li class="breadcrumb-item"><a href="javascript:(0);">کاربران</a></li>
                            <li class="breadcrumb-item active">پزشک</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="overflow-x: hidden">
                                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="dataTables_length" id="DataTables_Table_0_length"><label>نمایش
                                                    <select name="DataTables_Table_0_length"
                                                        aria-controls="DataTables_Table_0"
                                                        class="custom-select custom-select-sm form-control form-control-sm">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select> داده</label></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="datatable table table-hover table-center mb-0 dataTable no-footer"
                                                id="DataTables_Table_0" role="grid"
                                                aria-describedby="DataTables_Table_0_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 316.233px;" aria-sort="ascending"
                                                            aria-label="نام پزشک: activate to sort column descending">نام
                                                            پزشک</th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 142.233px;"
                                                            aria-label="تخصص: activate to sort column ascending">تخصص</th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 163.067px;"
                                                            aria-label="عضویت از: activate to sort column ascending">عضویت
                                                            از</th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 155.533px;"
                                                            aria-label="درآمد: activate to sort column ascending">درآمد</th>
                                                        <th class="sorting" tabindex="0"
                                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                            style="width: 214.933px;"
                                                            aria-label="وضعیت اکانت: activate to sort column ascending">
                                                            وضعیت اکانت</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($doctors) == 0)
                                                        <div class="alert alert-danger">
                                                            <p class="mb-0" style="font-size: 0.9rem">تخصصی وجود
                                                                ندارد</p>
                                                        </div>
                                                    @else
                                                        @foreach ($doctors as $doctor)
                                                            <tr role="row" class="odd">
                                                                <td class="sorting_1">
                                                                    <h2 class="table-avatar">
                                                                        <a href="profile.html"
                                                                            class="avatar avatar-sm mr-2"><img
                                                                                class="avatar-img rounded-circle"
                                                                                src="{{ $doctor->avatar ? asset('storage/photos/' . $doctor->avatar) : asset('front/img/avatar-default-icon.png') }}"
                                                                                alt="User Image"></a>
                                                                        <a href="profile.html">پزشک {{ $doctor->name }}</a>
                                                                    </h2>
                                                                </td>
                                                                <td>دندان پزشک</td>

                                                                <td>14 آبان 1399 <br><small>02.59 قبل از ظهر</small></td>

                                                                <td>{{ ($doctor->costs == 'free') ? "رایگان" : $doctor->costs }}</td>

                                                                <td>
                                                                    <div class="status-toggle">
                                                                        <input type="checkbox" id="status_1"
                                                                            class="check" checked="">
                                                                        <label for="status_1"
                                                                            class="checktoggle">checkbox</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{-- <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="DataTables_Table_0_info" role="status"
                                                aria-live="polite">نمایش 1 تا 10 از 10 داده</div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                id="DataTables_Table_0_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="DataTables_Table_0_previous"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0"
                                                            class="page-link">قبلی</a></li>
                                                    <li class="paginate_button page-item active"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0"
                                                            class="page-link">1</a></li>
                                                    <li class="paginate_button page-item next disabled"
                                                        id="DataTables_Table_0_next"><a href="#"
                                                            aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0"
                                                            class="page-link">بعدی</a></li>
                                                </ul>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
