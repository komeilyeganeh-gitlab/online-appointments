@extends('administrator.layouts.master')

@section('title', 'پروفایل')

@section('content')
    <div class="page-wrapper" style="min-height: 534px;">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">پروفایل</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">دشبرد</a></li>
                            <li class="breadcrumb-item active">پروفایل</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-header">
                        <div class="row align-items-center">
                            <div class="col-auto profile-image">
                                <a href="#">
                                    <img class="rounded-circle" alt="User Image"
                                        src="{{ asset('administrator/img/profiles/avatar-01.jpg') }}">
                                </a>
                            </div>
                            <div class="col ml-md-n2 profile-user-info">
                                <h4 class="user-name mb-0">{{ $admin->name }}</h4>
                                <h6 class="text-muted">{{ $admin->email }}</h6>
                                <div class="user-Location"><i class="fa fa-map-marker"></i>{{ $admin->city }} -
                                    {{ $admin->province }}</div>
                                <div class="about-text">{{ $admin->about_me }}</div>
                            </div>
                            <div class="col-auto profile-btn">

                                <a href="#edit_personal_details" class="btn btn-primary" data-toggle="modal"
                                    style="font-weight: bold;">
                                    ویرایش
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#per_details_tab">درباره ما</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#password_tab">رمزعبور</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content profile-tab-cont">

                        <!-- Personal Details Tab -->
                        <div class="tab-pane fade show active" id="per_details_tab">

                            <!-- Personal Details -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title d-flex justify-content-between">
                                                <span> مشخصات شخصی</span>
                                                <div id="success-edit-profile" class="w-50"></div>
                                                {{-- <a class="edit-link" data-toggle="modal"
                                                    href="#edit_personal_details"><i class="fa fa-edit mr-1"></i> ویرایش
                                                </a> --}}
                                            </h5>
                                            <div class="row">
                                                <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">نام</p>
                                                <p class="col-sm-10">{{ $admin->name }}</p>
                                            </div>
                                            <div class="row">
                                                <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">تاریخ تولد</p>
                                                <p class="col-sm-10">{{ $admin->birthday }}</p>
                                            </div>
                                            <div class="row">
                                                <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">ایمیل</p>
                                                <p class="col-sm-10">{{ $admin->email }}</p>
                                            </div>
                                            <div class="row">
                                                <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">موبایل</p>
                                                <p class="col-sm-10">{{ $admin->phone }}</p>
                                            </div>
                                            <div class="row">
                                                <p class="col-sm-2 text-muted text-sm-right mb-0">آدرس</p>
                                                <p class="col-sm-10 mb-0">{{ $admin->country }},<br>
                                                    {{ $admin->province }},<br>
                                                    {{ $admin->city }},<br>
                                                    {{ $admin->address }} - {{ $admin->postal_code }}<br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @if (isset('#edit_personal_details'))
                                        echo '$(window).load(function(){$("#' . $modal . '").modal(\'show\');});';
                                    @endif --}}
                                    <!-- Edit Details Modal -->
                                    <div class="modal fade" id="edit_personal_details" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"> مشخصات شخصی</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="form-edit-profile">
                                                        <div class="row form-row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>نام و نام خانوادگی</label>
                                                                    <input type="text" class="form-control"
                                                                        value="{{ $admin->name }}" name="name" id="name">

                                                                    <span class="text-danger" id="error-name"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>تاریخ تولد</label>
                                                                    <div class="cal-icon">
                                                                        <input type="text" class="form-control"
                                                                            value="{{ $admin->birthday }}"
                                                                            name="birthday" id="birthday"
                                                                            style="padding-right: 35px">

                                                                        <span class="text-danger" id="error-birthday"
                                                                            style="font-size: 0.8rem"></span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>ایمیل</label>
                                                                    <input type="email" class="form-control"
                                                                        value="{{ $admin->email }}" name="email"
                                                                        id="email">

                                                                    <span class="text-danger" id="error-email"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>موبایل</label>
                                                                    <input type="text" value="{{ $admin->phone }}"
                                                                        class="form-control" name="phone" id="phone">

                                                                    <span class="text-danger" id="error-phone"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>درباره من</label>
                                                                    <textarea name="about" id="about"
                                                                        class="form-control">{{ $admin->about_me }}</textarea>

                                                                    <span class="text-danger" id="error-about"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <h5 class="form-title"><span>آدرس</span></h5>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label>آدرس</label>
                                                                    <input type="text" class="form-control"
                                                                        value="{{ $admin->address }}" name="address"
                                                                        id="address">

                                                                    <span class="text-danger" id="error-address"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>شهر</label>
                                                                    <input type="text" class="form-control"
                                                                        value="{{ $admin->city }}" name="city" id="city">

                                                                    <span class="text-danger" id="error-city"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>استان</label>
                                                                    <input type="text" class="form-control"
                                                                        value="{{ $admin->province }}" name="province"
                                                                        id="province">

                                                                    <span class="text-danger" id="error-province"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>کد پستی</label>
                                                                    <input type="text" class="form-control "
                                                                        value="{{ $admin->postal_code }}"
                                                                        name="postal_code" id="postal_code">

                                                                    <span class="text-danger" id="error-postal_code"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label>کشور</label>
                                                                    <input type="text" class="form-control "
                                                                        value="{{ $admin->country }}" name="country"
                                                                        id="country">

                                                                    <span class="text-danger" id="error-country"
                                                                        style="font-size: 0.8rem"></span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit"
                                                            class="btn btn-primary btn-block button-submit">ذخیره
                                                            تغییرات</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Edit Details Modal -->

                                </div>


                            </div>
                            <!-- /Personal Details -->

                        </div>
                        <!-- /Personal Details Tab -->

                        <!-- Change Password Tab -->
                        <div id="password_tab" class="tab-pane fade">

                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">‌تغییر رمز عبور</h5>
                                    <div class="row">
                                        <div class="col-md-10 col-lg-6">
                                            <div class="display-error"></div>
                                            <form action="{{ route('admin.change-password') }}" method="POST"
                                                id="edit-password">
                                                @csrf
                                                <div class="form-group">
                                                    <label>رمزعبور قبل</label>
                                                    <input type="password" class="form-control" name="oldpass" id="oldpass">
                                                </div>
                                                <div class="form-group">
                                                    <label>رمزعبور جدید</label>
                                                    <input type="password" class="form-control" name="password" id="pass">
                                                </div>
                                                <div class="form-group">
                                                    <label>تایید رمزعبور</label>
                                                    <input type="password" class="form-control" name="confirm_password" id="conf_pass">
                                                </div>
                                                <button class="btn btn-primary btn-ch-password" type="submit" style="font-weight: bold">ذخیره
                                                    تغییرات</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Change Password Tab -->

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(() => {

            // ---------- edit information with ajax ----------
            $(".button-submit").click((e) => {
                e.preventDefault();
                var formEditInfo = $("#form-edit-profile");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('admin.profile') }}",
                    type: 'POST',
                    data: formEditInfo.serialize(),
                    success: (res) => {
                        var inputs = ['name', 'birthday', 'email', 'phone', 'about', 'address',
                            'city', 'province', 'postal_code', 'country'
                        ];
                        for (inp of inputs) {
                            $("#error-" + inp).html('');
                            $("#" + inp).removeClass("border-danger");
                        }
                        $("#edit_personal_details").removeClass("show");
                        $(".modal-backdrop").removeClass("show");
                        $("#success-edit-profile").html(`
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="font-size:0.88rem">
                            اطلاعات با موفقیت ویرایش شد
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        `);
                    },
                    error: (err) => {
                        if (err.status == 422) {
                            var errors = err.responseJSON.errors;
                            var inputs = ['name', 'birthday', 'email', 'phone', 'about',
                                'address', 'city', 'province', 'postal_code', 'country'
                            ];
                            for (e in errors) {
                                for (inp of inputs) {
                                    if (inp in errors) {
                                        $("#error-" + inp).html(errors[inp]);
                                        $("#" + inp).addClass("border-danger");
                                    } else {
                                        $("#error-" + inp).html('');
                                        $("#" + inp).removeClass("border-danger");
                                    }
                                }

                            }
                        }
                    }
                });
            });

            // ---------- change password with ajax ----------
            $(".btn-ch-password").click((e) => {
                e.preventDefault();
                var formChPassword = $("#edit-password");

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: formChPassword.attr('action'),
                    type: formChPassword.attr('method'),
                    data: formChPassword.serialize(),
                    success: (res) => {
                        $(".display-error").html("");
                        $("#oldpass").val("");
                        $("#pass").val("");
                        $("#conf_pass").val("");
                        $(".display-error").append(`
                                <div class="alert alert-success" style="font-size: 0.8rem; font-weight:bold;">
                                    ${res.success}
                                </div>
                        `);
                    },
                    error: (err) => {
                        var errors = err.responseJSON.errors;
                        $(".display-error").html("");
                        for (er in errors) {
                            $(".display-error").append(`
                                <div class="alert alert-danger" style="font-size: 0.8rem;font-weight:bold;">
                                    ${errors[er]}
                                </div>
                            `);
                        }
                        
                    }
                });
            });
        });
    </script>
@endsection
