<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>داک کیور - @yield('title','value')</title>

    <!-- Favicons -->
    <link type="image/x-icon" href="{{ asset('front/img/favicon.png') }}" rel="icon">

    <link rel="stylesheet" href="{{ asset('front/all.min.css') }}">

    <style id="theia-sticky-sidebar-stylesheet-TSS">.theiaStickySidebar:after {content: ""; display: table; clear: both;}</style>

</head>

<body>

<!-- Main Wrapper -->
<div class="main-wrapper">

    @include('layouts.header')

    @yield('content')

    @include('layouts.footer')

</div>
<!-- /Main Wrapper -->

<script src="{{ asset('front/all.min.js') }}"></script>

</body>

</html>