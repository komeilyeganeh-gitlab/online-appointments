{{-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('front/all.min.css') }}">
</head>

<body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- header -->
        <header class="header">
            <nav class="navbar navbar-expand-lg header-nav">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{ route('home') }}" class="navbar-brand logo">
                        <img src="{{ asset('front/img/logo.png') }}" class="img-fluid" alt="logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{ route('home') }}" class="menu-logo">
                            <img src="{{ asset('front/img/logo.png') }}" class="img-fluid" alt="logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">
                        <li>
                            <a href="index.html">خانه</a>
                        </li>
                        <li class="has-submenu">
                            <a href="#">پزشک‌ها<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="doctor-dashboard.html">دشبرد پزشک</a></li>
                                <li><a href="appointments.html">نوبت‌دهی</a></li>
                                <li><a href="schedule-timings.html">زمان‌بندی</a></li>
                                <li><a href="my-patients.html">لیست بیماران</a></li>
                                <li><a href="patient-profile.html">پروفایل بیماران</a></li>
                                <li><a href="chat-doctor.html">چت</a></li>
                                <li><a href="invoices.html">صورت‌حساب</a></li>
                                <li><a href="doctor-profile-settings.html">تنظیمات پروفایل</a></li>
                                <li><a href="reviews.html">نظرات</a></li>
                                <li><a href="doctor-register.html">ثبت‌نام پزشک</a></li>
                                <li class="has-submenu">
                                    <a href="doctor-blog.html">بلاگ</a>
                                    <ul class="submenu">
                                        <li><a href="doctor-blog.html">بلاگ</a></li>
                                        <li><a href="blog-details.html">مشاهده بلاگ</a></li>
                                        <li><a href="doctor-add-blog.html">افزودن بلاگ</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">مراجعه‌کنندگان<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li class="has-submenu">
                                    <a href="#">پزشکان</a>
                                    <ul class="submenu">
                                        <li><a href="map-grid.html">گرید نقشه</a></li>
                                        <li><a href="map-list.html">لیست نقشه</a></li>
                                    </ul>
                                </li>
                                <li><a href="search.html">جستجو پزشک</a></li>
                                <li><a href="doctor-profile.html">پروفایل پزشک</a></li>
                                <li><a href="booking.html">رزرو نوبت</a></li>
                                <li><a href="checkout.html">پرداخت</a></li>
                                <li><a href="booking-success.html">رزرو موفق</a></li>
                                <li><a href="patient-dashboard.html">دشبرد مراجعه‌کننده</a></li>
                                <li><a href="favourites.html">‌علاقه‌مندیها</a></li>
                                <li><a href="chat.html">چت</a></li>
                                <li><a href="profile-settings.html">تنظیمات پروفایل</a></li>
                                <li><a href="change-password.html">‌تغییر رمز عبور</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">‌داروخانه<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="pharmacy-index.html">‌داروخانه</a></li>
                                <li><a href="pharmacy-details.html">‌جزییات داروخانه</a></li>
                                <li><a href="pharmacy-search.html">‌جستجو داروخانه</a></li>
                                <li><a href="product-all.html">‌محصولات</a></li>
                                <li><a href="product-description.html">توضیحات محصول</a></li>
                                <li><a href="cart.html">‌سبد خرید</a></li>
                                <li><a href="product-checkout.html">‌خرید محصولات</a></li>
                                <li><a href="payment-success.html">‌پرداخت موفق</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu active">
                            <a href="#">‌صفحات<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="voice-call.html">‌تماس صوتی</a></li>
                                <li><a href="video-call.html">‌تماس تصویری</a></li>
                                <li><a href="search.html">‌جستجو پزشک</a></li>
                                <li><a href="calendar.html">‌تقویم</a></li>

                                <li><a href="components.html">‌کامپوننت‌ها</a></li>
                                <li class="has-submenu">
                                    <a href="invoices.html">صورت‌حساب</a>
                                    <ul class="submenu">
                                        <li><a href="invoices.html">صورت‌حساب</a></li>
                                        <li><a href="invoice-view.html">‌مشاهده صورت‌حساب</a></li>
                                    </ul>
                                </li>
                                <li><a href="blank-page.html">‌صفحه شروع</a></li>
                                <li><a href="login.html">‌ورود</a></li>
                                <li class="active"><a href="register.html">‌ثبت‌نام</a></li>
                                <li><a href="forgot-password.html">‌فراموشی رمزعبور</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#">‌بلاگ<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="blog-list.html">‌لیست بلاگ</a></li>
                                <li><a href="blog-grid.html">‌گرید بلاگ</a></li>
                                <li><a href="blog-details.html">‌جزییات بلاگ</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="#" target="_blank">ادمین<i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu">
                                <li><a href="admin/index.html" target="_blank">‌ادمین</a></li>
                                <li><a href="pharmacy/index.html" target="_blank">ادمین داروخانه</a></li>
                            </ul>
                        </li>
                        <li class="login-link">
                            <a href="login.html">‌ورود / ثبت‌نام</a>
                        </li>
                    </ul>
                </div>
                <ul class="nav header-navbar-rht">
                    <li class="nav-item contact-item">
                        <div class="header-contact-img">
                            <i class="far fa-hospital"></i>
                        </div>
                        <div class="header-contact-detail">
                            <p class="contact-header">‌تماس با ما</p>
                            <p class="contact-info-header"> +1 315 369 5943</p>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-login" href="login.html">ورود / ثبت‌نام</a>
                    </li>
                </ul>
            </nav>
        </header>
        <!-- /header -->

        <!-- page content -->
        <div class="content">
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer -->
        <footer class="footer">

            <!-- footer top -->
            <div class="footer-top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">

                            <!-- footer widget -->
                            <div class="footer-widget footer-about">
                                <div class="footer-logo">
                                    <img src="{{ asset('front/img/footer-logo.png') }}" alt="logo">
                                </div>
                                <div class="footer-about-content">
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                        گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                                    </p>
                                    <div class="social-icon">
                                        <ul>
                                            <li>
                                                <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank"><i class="fab fa-dribbble"></i> </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /footer widget -->

                        </div>

                        <div class="col-lg-3 col-md-6">

                            <!-- footer widget -->
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">برای مراجعه‌کنندگان</h2>
                                <ul>
                                    <li><a href="search.html">جستجو پزشکان</a></li>
                                    <li><a href="login.html">‌ورود</a></li>
                                    <li><a href="register.html">‌ثبت‌نام</a></li>
                                    <li><a href="booking.html">رزرو نوبت</a></li>
                                    <li><a href="patient-dashboard.html">دشبرد مراجعه‌کننده</a></li>
                                </ul>
                            </div>
                            <!-- /footer widget -->

                        </div>

                        <div class="col-lg-3 col-md-6">

                            <!-- footer widget -->
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">برای پزشکان</h2>
                                <ul>
                                    <li><a href="appointments.html">نوبت‌دهی</a></li>
                                    <li><a href="chat.html">چت</a></li>
                                    <li><a href="login.html">‌ورود</a></li>
                                    <li><a href="doctor-register.html">‌ثبت‌نام</a></li>
                                    <li><a href="doctor-dashboard.html">دشبرد پزشک</a></li>
                                </ul>
                            </div>
                            <!-- /footer widget -->

                        </div>

                        <div class="col-lg-3 col-md-6">

                            <!-- footer widget -->
                            <div class="footer-widget footer-contact">
                                <h2 class="footer-title">ارتباط با ما</h2>
                                <div class="footer-contact-info">
                                    <div class="footer-address">
                                        <span><i class="fas fa-map-marker-alt"></i></span>
                                        <p> خیابان ارم، سانفرانسیسکو<br> کالیفرنیا خیابان 94108 </p>
                                    </div>
                                    <p>
                                        <i class="fas fa-phone-alt"></i>
                                        +1 315 369 5943
                                    </p>
                                    <p class="mb-0">
                                        <i class="fas fa-envelope"></i>
                                        doccure@example.com
                                    </p>
                                </div>
                            </div>
                            <!-- /footer widget -->

                        </div>

                    </div>
                </div>
            </div>
            <!-- /footer top -->

            <!-- footer bottom -->
            <div class="footer-bottom">
                <div class="container-fluid">

                    <!-- copyright -->
                    <div class="copyright">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="copyright-text">
                                    <p class="mb-0">&copy; 1399 داک‌کیور، تمامی حقوق محفوظ است.</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">

                                <!-- copyright menu -->
                                <div class="copyright-menu">
                                    <ul class="policy-menu">
                                        <li><a href="term-condition.html">شرایط و مقررات</a></li>
                                        <li><a href="privacy-policy.html">حریم شخصی</a></li>
                                    </ul>
                                </div>
                                <!-- /copyright menu -->

                            </div>
                        </div>
                    </div>
                    <!-- /copyright -->

                </div>
            </div>
            <!-- /footer bottom -->

        </footer>
        <!-- /footer -->

    </div>
    <!-- /Main Wrapper -->


    <script src="{{ asset('front/all.min.js') }}"></script>

</body>

</html>


<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>

</html> --}}
