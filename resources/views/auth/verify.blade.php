@extends('layouts.master')

@section('title', 'تایید ایمیل')

@section('content')
<div class="container pt-5 pb-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-danger font-weight-bold"> لطفا ایمیل خود را تایید کنید</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            لینک جدید به ایمیل شما ارسال شد.
                        </div>
                    @endif

                     برای ادامه کار، لطفا ایمیل خود را تایید کنید. <br><br>
                    اگر ایمیلی دریافت نکردید,
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">اینجا کلیک کنید</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
