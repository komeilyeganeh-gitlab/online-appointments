@extends('layouts.master')

@section('title', 'ورود پزشک ')

@section('content')
    <div class="container-fluid pt-4 pb-4">

        <div class="row">
            <div class="col-md-8 offset-md-2">

                <!-- Login  محتوای تب -->
                <div class="account-content">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 col-lg-6 login-left">
                            <img src="{{ asset('front/img/login-banner.png') }}" class="img-fluid"
                                alt="Doccure Login">
                        </div>
                        <div class="col-md-12 col-lg-6 login-right">
                            <div class="login-header">
                                <h3> ورود <span>پزشک</span></h3>
                            </div>
                            <form action="{{ route('auth.doctor-login') }}" method="POST">
                                @csrf
                                <div class="form-group form-focus">
                                    <input type="email" class="form-control floating @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" autofocus>
                                    <label class="focus-label">ایمیل</label>
                                </div>

                                <div class="form-group form-focus">
                                    <input type="password"
                                        class="form-control floating @error('password') is-invalid @enderror"
                                        name="password">
                                    <label class="focus-label">رمز عبور</label>
                                </div>
                                @error('email')
                                    <div class="alert alert-danger text-small font-weight-bold" style="font-size: 0.84rem">{{ $message }}</div>
                                @enderror
                                @error('password')
                                    <div class="alert alert-danger text-small font-weight-bold" style="font-size: 0.84rem">{{ $message }}</div>
                                @enderror
                                <div class="text-right">
                                    <a class="forgot-link" href="{{ route('password.request') }}">فراموشی رمزعبور</a>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg login-btn font-weight-bold"
                                    type="submit">‌ورود</button>
                                <div class="login-or">
                                    <span class="or-line"></span>
                                    <span class="span-or">یا</span>
                                </div>
                                <div class="row form-row social-login">
                                    <div class="col-12">
                                        <a href="{{ route('auth.google') }}" class="btn btn-google btn-block">گوگل <i
                                                class="fab fa-google mr-1"></i></a>
                                    </div>
                                </div>
                                <div class="text-center dont-have"> اکانت ندارید؟ <a href="{{ route('auth.doctor.register') }}"
                                        class="text-info font-weight-bold">‌ثبت‌ نام</a></div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Login  محتوای تب -->

            </div>
        </div>

    </div>
@endsection
