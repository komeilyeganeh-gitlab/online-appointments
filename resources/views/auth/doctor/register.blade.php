@extends('layouts.master')

@section('title', 'ثبت نام')

@section('content')
    <div class="container-fluid pt-4 pb-4">

        <div class="row">
            <div class="col-md-8 offset-md-2">

                <!-- register content -->
                <div class="account-content">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 col-lg-6 login-left">
                            <img src="{{ asset('front/img/login-banner.png') }}" class="img-fluid" alt="doccure register">
                        </div>
                        <div class="col-md-12 col-lg-6 login-right">
                            <div class="login-header">
                                <h3>ثبت نام پزشک<a href="{{ route('register') }}" class="text-info font-weight-bold">مراجعه کننده هستید؟</a></h3>
                            </div>

                            <!-- register form -->
                            <form action="{{ route('auth.doctor-register') }}" method="POST">
                                @csrf
                                <div class="form-group form-focus">
                                    <input type="text" class="form-control floating" name="name" value="{{ old('name') }}" autofocus>
                                    <label class="focus-label">نام پزشک</label>
                                </div>
                                @error('name')
                                    <div class="alert alert-danger text-danger text-small" style="font-size: 0.86rem">{{ $message }}</div>
                                @enderror
                                <div class="form-group form-focus">
                                    <input type="email" class="form-control floating" name="email" value="{{ old('email') }}">
                                    <label class="focus-label">ایمیل</label>
                                </div>
                                @error('email')
                                    <div class="alert alert-danger text-danger text-small" style="font-size: 0.86rem">{{ $message }}</div>
                                @enderror
                                <div class="form-group form-focus">
                                    <input type="password" class="form-control floating" name="password">
                                    <label class="focus-label">رمز عبور</label>
                                </div>
                                <div class="form-group form-focus">
                                    <input type="password" class="form-control floating" name="password_confirmation">
                                    <label class="focus-label">تکرار رمز عبور</label>
                                </div>
                                @error('password')
                                    <div class="alert alert-danger text-danger text-small" style="font-size: 0.86rem">{{ $message }}</div>
                                @enderror
                                <div class="text-right">
                                    <a class="forgot-link" href="{{ route('auth.doctor.login') }}">اکانت دارید؟</a>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg login-btn font-weight-bold" type="submit">ثبت‌ نام
                                </button>
                                <div class="login-or">
                                    <span class="or-line"></span>
                                    <span class="span-or">یا</span>
                                </div>
                                <div class="row form-row social-login">
                                    <div class="col-12">
                                        <a href="{{ route('auth.google') }}" class="btn btn-google btn-block">گوگل <i
                                                class="fab fa-google mr-1"></i></a>
                                    </div>
                                </div>
                            </form>
                            <!-- /register form -->

                        </div>
                    </div>
                </div>
                <!-- /register content -->

            </div>
        </div>

    </div>
@endsection
