@extends('layouts.master')

@section('title', 'فراموشی رمز عبور')

@section('content')
    <div class="container-fluid mt-4 mb-4">

        <div class="row">
            <div class="col-md-8 offset-md-2">

                <!-- Account Content -->
                <div class="account-content">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 col-lg-6 login-left">
                            <img src="{{ asset('front/img/login-banner.png') }}" class="img-fluid" alt="Login Banner">
                        </div>

                        <div class="col-md-12 col-lg-6 login-right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="login-header">
                                <h3> فراموشی رمز عبور؟</h3>
                                <p class="small text-muted">ایمیل خود را وارد کنید تا مجدد رمز عبور را دریافت کنید</p>
                            </div>

                            <!-- Forgot Password Form -->
                            <form action="{{ route('password.email') }}" method="POST">
                                @csrf
                                <div class="form-group form-focus">
                                    <input type="email" class="form-control floating @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" autocomplete="email" autofocus>
                                    <label class="focus-label">ایمیل</label>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="text-right">
                                    <a class="forgot-link" href="{{ route('login') }}">رمز عبور خود را به خاطر می
                                        آورید؟</a>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg login-btn" type="submit">بازیابی
                                    رمزعبور</button>
                            </form>
                            <!-- /Forgot Password Form -->

                        </div>
                    </div>
                </div>
                <!-- /Account Content -->

            </div>
        </div>

    </div>

@endsection