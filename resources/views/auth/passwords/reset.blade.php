@extends('layouts.master')

@section('title', 'رمز عبور جدید')

@section('content')
    <div class="container-fluid mt-4 mb-4">

        <div class="row">
            <div class="col-md-8 offset-md-2">

                <!-- Account Content -->
                <div class="account-content">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-7 col-lg-6 login-left">
                            <img src="{{ asset('front/img/login-banner.png') }}" class="img-fluid" alt="Login Banner">
                        </div>

                        <div class="col-md-12 col-lg-6 login-right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="card">
                                 <div class="card-header bg-light">بازنشانی رمز عبور</div>
                            </div>
                           
                            <!-- Forgot Password Form -->
                            <form action="{{ route('password.update') }}" method="POST">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group form-focus">
                                    <input type="email" class="form-control floating @error('email') is-invalid @enderror" name="email"
                                        value="{{ $email ?? old('email') }}" autocomplete="email" autofocus>
                                    <label class="focus-label">ایمیل</label>
                                </div>
                                <div class="form-group form-focus">
                                    <input type="password" class="form-control floating @error('password') is-invalid @enderror" name="password"
                                         autocomplete="new-password">
                                    <label class="focus-label">رمز عبور جدید</label>
                                </div>
                                <div class="form-group form-focus">
                                    <input type="password" class="form-control floating" name="password_confirmation"
                                         autocomplete="new-password">
                                    <label class="focus-label"> تکرار رمز عبور جدید</label>
                                </div>
                                @error('email')
                                    <div class="alert alert-danger text-small font-weight-bold" style="font-size: 0.84rem">{{ $message }}</div>
                                @enderror
                                @error('password')
                                    <div class="alert alert-danger text-small font-weight-bold" style="font-size: 0.84rem">{{ $message }}</div>
                                @enderror
                                <button class="btn btn-primary btn-block btn-lg login-btn font-weight-bold" type="submit">تایید رمز عبور</button>
                            </form>
                            <!-- /Forgot Password Form -->

                        </div>
                    </div>
                </div>
                <!-- /Account Content -->

            </div>
        </div>

    </div>

@endsection

