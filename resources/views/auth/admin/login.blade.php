<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title> داک‌کیور - ورود مدیر</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

        <link rel="stylesheet" href="{{ asset('administrator/all.min.css') }}">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper login-body">
            <div class="login-wrapper">
            	<div class="container">
                	<div class="loginbox">
                    	<div class="login-left">
							<img class="img-fluid" src="{{ asset('administrator/img/logo-white.png') }}" alt="Logo">
                        </div>
                        <div class="login-right">
							<div class="login-right-wrap">
								<h1>‌ورود</h1>
								<p class="account-subtitle">دسترسی به پنل مدیریت</p>
								
								<!-- Form -->
								<form action="{{ route('login.admin') }}" method="POST">
									@csrf
									<div class="form-group">
										<input class="form-control" type="text" placeholder="ایمیل" name="email">
									</div>
									<div class="form-group">
										<input class="form-control" type="password" placeholder="رمزعبور" name="password">
									</div>
									<div class="form-group">
										<button class="btn btn-primary btn-block" type="submit">‌ورود</button>
									</div>
								</form>
								<!-- /Form -->
								
								<div class="text-center forgotpass"><a href="forgot-password.html"> فراموشی رمز عبور؟</a></div>
								{{-- <div class="login-or">
									<span class="or-line"></span>
									<span class="span-or">یا</span>
								</div>
								  
								<!-- Social Login -->
								<div class="social-login">
									<span>  ورود با </span>
									<a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google"></i></a>
								</div>
								<!-- /Social Login --> --}}
								
								{{-- <div class="text-center dont-have">اکانت ندارید؟<a href="register.html">‌ثبت‌نام</a></div> --}}
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Main Wrapper -->
	
		<script src="{{ asset('administrator/all.min.js') }}"></script>
		
    </body>
</html>