<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'رمز عبور شما بازنشانی شد!',
    'sent' => 'لینک بازنشانی رمز عبور به ایمیل شما ارسال گردید.',
    'throttled' => 'لطفاً قبل از تلاش مجدد، صبر کنید.',
    'token' => 'رمز موردنظر برای بازنشانی معتیر نیست',
    'user' => "ما نمی توانیم کاربری با این آدرس ایمیل پیدا کنیم.",

];
