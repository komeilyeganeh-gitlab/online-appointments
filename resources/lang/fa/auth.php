<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات وارد شده با داده های سیستم مطابقت ندارد',
    'password' => 'رمز عبور اشتباه می باشد',
    'throttle' => 'تلاش برای ورود بسیار زیاد است. لطفاً بعد از : :seconds ثانیه دوباره امتحان کنید.',

];
