$(document).ready(function () {
    // ---------- Add speciality -----------
    $("#form-add-speciality").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            data: new FormData(this),
            type: 'POST',
            dataType: "JSON",
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    location.reload(true);
                }
            },
            error: function (err) {
                if (err.status != 200) {
                    var errors = err.responseJSON.errors;
                    $(".errors").html("");
                    for (er in errors) {
                        $(".errors").append(`
                        <div class="alert alert-danger small font-weight-bold">
                            ${errors[er]}    
                        </div>
                    `);
                    }
                }
            },
        });
    });

    // ---------- Edit speciality - Send Data To Modal -----------
    $(".btn-edit-speciality").each(function () {
        $(this).click(function () {
            var title = $(this).attr("data-title");
            var id = $(this).attr("data-id");

            $("#edit_specialities_details .title").attr("value", title);

            var action = "specialities/" + id;
            $("#edit_specialities_details .form-edit").attr("action", action);
        });
    });
    $("#form-edit-speciality").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: "POST",
            data: new FormData(this),
            dataType: "JSON",
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                location.reload(true);
            },
            error: function (err) {
                if (err.status != 200) {
                    var errors = err.responseJSON.errors;
                    for (er in errors) {
                        $(".errors-edit").html("");
                        $(".errors-edit").append(`
                        <div class="alert alert-danger small font-weight-bold">
                            ${errors[er]}    
                        </div>
                    `);
                    }
                }
            },
        });
    });

    // ---------- Delete Speciality ----------
    $(".btn-modal-delete").click(function() {
        var id = $(this).attr('data-id');
        var action = 'specialities/' + id;
        var form = $("#form-delete-speciality");
        $(form).attr('action', action);
    });
});
